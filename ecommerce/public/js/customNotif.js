/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/customNotif.js":
/*!*************************************!*\
  !*** ./resources/js/customNotif.js ***!
  \*************************************/
/***/ (() => {

eval("var customNotif = {\n  notif: function notif(status, message) {\n    var toastr = new CustomEvent('toastr', {\n      'detail': {\n        'status': status,\n        'message': message\n      }\n    });\n    return toastr;\n  }\n};\nwindow.customNotif = customNotif;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvY3VzdG9tTm90aWYuanMuanMiLCJuYW1lcyI6WyJjdXN0b21Ob3RpZiIsIm5vdGlmIiwic3RhdHVzIiwibWVzc2FnZSIsInRvYXN0ciIsIkN1c3RvbUV2ZW50Iiwid2luZG93Il0sInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvY3VzdG9tTm90aWYuanM/NTY3MSJdLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBjdXN0b21Ob3RpZiA9IHtcclxuICAgIG5vdGlmKHN0YXR1cywgbWVzc2FnZSkge1xyXG5cdFx0bGV0IHRvYXN0ciA9IG5ldyBDdXN0b21FdmVudCgndG9hc3RyJywge1xyXG5cdFx0XHQnZGV0YWlsJzoge1xyXG5cdFx0XHRcdCdzdGF0dXMnOiBzdGF0dXMsIFxyXG5cdFx0XHRcdCdtZXNzYWdlJzogbWVzc2FnZVxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHJcblx0XHRyZXR1cm4gdG9hc3RyO1xyXG5cdH1cclxufVxyXG5cclxud2luZG93LmN1c3RvbU5vdGlmID0gY3VzdG9tTm90aWY7Il0sIm1hcHBpbmdzIjoiQUFBQSxJQUFNQSxXQUFXLEdBQUc7RUFDaEJDLEtBQUssV0FBQUEsTUFBQ0MsTUFBTSxFQUFFQyxPQUFPLEVBQUU7SUFDekIsSUFBSUMsTUFBTSxHQUFHLElBQUlDLFdBQVcsQ0FBQyxRQUFRLEVBQUU7TUFDdEMsUUFBUSxFQUFFO1FBQ1QsUUFBUSxFQUFFSCxNQUFNO1FBQ2hCLFNBQVMsRUFBRUM7TUFDWjtJQUNELENBQUMsQ0FBQztJQUVGLE9BQU9DLE1BQU07RUFDZDtBQUNELENBQUM7QUFFREUsTUFBTSxDQUFDTixXQUFXLEdBQUdBLFdBQVcifQ==\n//# sourceURL=webpack-internal:///./resources/js/customNotif.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/customNotif.js"]();
/******/ 	
/******/ })()
;